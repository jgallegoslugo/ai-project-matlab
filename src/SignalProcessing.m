function sp = SignalProcessing()
    sp.correlate = @correlate;
    sp.curves = @curves;
end

function rho = correlate(x, y)
%Prerequirement: x and y must be of same length
%
%correlate uses private corr function and shifts y vector so that the highest correlation
%between x and y is returned. This is necessary if, for example, a user
%were to write a 1 starting from top to bottom and the next time they wrote it
%from bottom to top. 
    rhos = zeros(1, length(x));
    for shift = 1:length(x)
        rhos(shift) = corr(shift, x, y);
    end
    
    rho = max(abs(rhos));    
end

function rho = corr( shift, x, orig_y )
% correlate.m - How much are 2 signals alike
%   
% Usage: [rho] = correlate(k, x, y);
%   inputs: shift
%       x, y = the signals to correlate
%   outputs: rho = the correlation coefficient -1..0..1

    length_y = length(orig_y);

    if length_y ~= length(x)
        error('lengths of x and y must match')
    end

    y = [orig_y(shift+1:length_y), orig_y(1:shift)];

    N = length(x);

    sxx = x*x.' - sum(x)*sum(x)/N;
    syy = y*y.' - sum(y)*sum(y)/N;
    sxy = x*y.' - sum(x)*sum(y)/N;

    rho = sxy / sqrt(sxx*syy);

end

function indeces = curves(points,  threshold, displayPlot)
%This function takes in points vector of (x, y) coordinates and computes
%the curvature of those points that are greater than the double threshold.
%The following code was adapted from an example that I found online at
%http://www.mathworks.com/matlabcentral/answers/57194#answer_69185
    xVals = points(1, :);
    yVals = points(2, :);
    
    numberOfPoints = length(xVals);
    curvature = zeros(1, numberOfPoints);
    
    for t = 2 : numberOfPoints - 1
        x1 = xVals(t - 1);
        y1 = yVals(t - 1);
        x2 = xVals(t);
        y2 = yVals(t);
        x3 = xVals(t + 1);
        y3 = yVals(t + 1);
        curvature(t) = 2*abs((x2-x1)*(y3-y1)-(x3-x1)*(y2-y1)) / ...
        sqrt(((x2-x1)^2+(y2-y1)^2)*((x3-x1)^2+(y3-y1)^2)*((x3-x2)^2+(y3-y2)^2));
    end
    
    if displayPlot
        plot(0: numberOfPoints - 1, curvature)
    end
    %indeces where the curvature is greater than threshold
    indeces = find(curvature > threshold);
end
