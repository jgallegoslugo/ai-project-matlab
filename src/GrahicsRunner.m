clear all;
close all;

%Grab handle to functions in Graphics.m
f = Graphics;
sp = SignalProcessing;

%Set the drawing canvas size
%this function must not be called if imagemagick is not installed.
%You can install it on ubuntu by typing:
%sudo apt-get install imagemagick 
%if you don't want to install this, the canvas will be 300x300 by default
%
%drawingArea(...) returns 1 if size is set succesfully, 0 if it fails

%uncomment next line to resize canvas if imagemagick is installed
%f.drawingArea(300,300);

%draw image
[origPoints, dimensions] = f.draw();

%distanceVector(points, midpointX, midpointY) calculates and returns a distance
%vector from the center of the image to all (x, y) pairs in points using
%the traditional distance formula.
distance = f.distanceVector(origPoints, dimensions(1)/2, dimensions(2)/2);

%close all;

%set how many training samples you want to run
trainingSamples = 20;
%points = zeros(trainingSamples, 300, 300)
%dims = zeros(trainingSamples
temp = distance;

for i = 1:trainingSamples
    [points{i}, dims{i}] = f.draw();
    distances{i} = f.distanceVector(points{i}, dims{i}(1)/2, dims{i}(2)/2);

    [temp, distances{i}] = f.padding(distance, distances{i});

    cor(i) = sp.correlate(temp, distances{i});
end

subplot(1, trainingSamples + 1, 1);
plot(origPoints(1,:), origPoints(2,:));

for i = 2:(trainingSamples + 1)
   subplot(1, trainingSamples + 1, i);
   plot(points{i-1}(1,:), points{i-1}(2,:))
   legend(['corr = ' num2str(cor(i-1))]);
end





