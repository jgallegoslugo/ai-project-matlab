function funcs = Graphics
    funcs.drawingArea = @drawingArea;
    funcs.draw = @draw;
    funcs.distanceVector = @distanceVector;
    funcs.padding = @padding;
end

function result = drawingArea(varargin)
%drawingArea creates an image in the file system named canvas.png of a
%specified size and at an optional specified path
%   possible calls are:
%   area = funcs.drawingArea(width-int, height-int, path-string)
%   area = funcs.drawingArea(width-int, height-int)
%   area = funcs.drawingArea(dimension-int, path-string)
%   area = funcs.drawingArea(demension-int)

    width = 0;
    height = 0;
    
    %if two arguments were passed into the function
    if nargin == 1
        if isnumeric(varargin{1})
            width = varargin{1};
            height = varargin{1};
            path = 'cd /home/jose/Documents/AI_API/';
        else
            'drawingArea takes width, height, path (optional) or size, path (optional) or size'
            result = 0;
            return 
        end
    elseif nargin == 2
        %if args are numbers
        if isnumeric(varargin{1}) && isnumeric(varargin{2})
            width = varargin{1};
            height = varargin{2};
            path = 'cd /home/jose/Documents/AI_API/';
        %else there is only one number and a path
        elseif isnumeric(varargin{1})
            width = varargin{1};
            height = varargin{1};
            path = varargin{2};
        else
            'drawingArea takes width, height, path (optional) or size, path (optional) or size'
            result = 0;
            return 
        end
    elseif nargin == 3
        if isnumeric(varargin{1}) && isnumeric(varargin{2})
            width = varargin{1};
            height = varargin{2};
            path = ['cd ' varargin{3}];
        end
    else
        'drawingArea takes width, height, path (optional) or size, path (optional) or size'
        result = 0;
        return
    end
    
    image = ' canvas.png canvas.png'; 
        
    resizeCommand = [path ' && convert -resize ' int2str(width) 'x' int2str(height) image];
    
    system(resizeCommand);
    
    result = 1;
end

function [points, picSize] = draw()
    close all %make sure all previous plots are closed

    figure, imshow('canvas.png');
    img = imread('canvas.png');
    h = imfreehand(gca, 'Closed', 0);

    points = h.getPosition;
    len = length(points);

    [width, height] = size(img);

    x = points(1:len, 1)'; y = height - points(1:len, 2)';
    points = [x; y];
    picSize = [width height];
end

function distances = distanceVector(points, midpointX, midpointY)
    len = length(points(1,:));
    x = points(1, :);
    y = points(2, :);
    
    distances(1:len) = sqrt((x - midpointX).^2 + (y - midpointY).^2); 
end

function [A, B] = padding(a, b)

    aLen = length(a); bLen = length(b); 
    
    if aLen > bLen
        i = 1:bLen/aLen:bLen+1;
        B = b(round(i(1:aLen)));
        A = a;
    elseif aLen < bLen
        i = 1:aLen/bLen:aLen+1;
        A = a(round(i(1:bLen)));
        B = b;
    else
        A = a; B = b;
    end
end






