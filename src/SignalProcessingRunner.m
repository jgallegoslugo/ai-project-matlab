clear all;
close all;

%Grab handle to functions in Graphics.m
f = Graphics;
[points, dimensions] = f.draw();
title(gca, 'Draw');

%Grab handle to functions in SignalProcessing.m
sp = SignalProcessing;

%Set displayCurvaturePlot = 1 so that the curvature plot is shown. Set to 0
%if you do not want it displayed.
displayCurvaturePlot = 1;
subplot(5, 1, 1);
numCurves = length(sp.curves(points, .2, displayCurvaturePlot));
legend('curve plot');

%Create two exactly the same curves to test that correlation is equal to 1
first = cos(2*pi*rand(1,50));
second = first;
third = cos(2*pi*rand(1,50));
fourth = cos(2*pi*rand(1,50));

subplot(5, 1, 2);
plot(first);

subplot(5, 1, 3);
plot(second);
cor = sp.correlate(first, second);
legend(['correlation of first and second graph' '= ' num2str(cor)]);

subplot(5, 1, 4);
plot(third);

subplot(5, 1, 5);
plot(fourth);
cor2 = sp.correlate(third, fourth);
legend(['correlation of third and fourth ' '= ' num2str(cor2)]);

